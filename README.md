![SensorSpace Logo](https://sensorspace.tech/images/logo1_vertical.png)

# Flathead Lake Biological Station - Web Server for Plotting Data in Real-Time
In this example we will be setting up a web server from scratch, which will be used to plot sensor data in real-time. The server will retrieve data from The Things Network using Node-RED, store the data using InfluxDB, and plot the data using Grafana. The web page with the live data will be served by Apache.

## 1.) Creating Your Web Server
The first thing we need to do is choose a company to host your web server. A popular choice is DigitalOcean - this is the company we will be using for this tutorial. Create an account: https://www.digitalocean.com/

Once you've created an account, click on **Create->Droplets** at the top right of the DigitalOcean website. DigitalOcean calls their servers "Droplets" (it's just a marketing thing...).

![enter image description here](https://s22.postimg.cc/6w25kc55d/image.png)

Then click on **One-click apps** and click on **LAMP on 16.04**. This is an image of a pre-configured server DigitalOcean has setup for us to make our lives easier. LAMP stands for Linux, Apache, mySQL and PHP, and 16.04 is the version of Ubuntu that all of this will be running on.

![enter image description here](https://s22.postimg.cc/66jd82rs1/image.png)

Scroll down and select the $10/month server option.

![enter image description here](https://s22.postimg.cc/jbyvdpo35/image.png)

Then choose the location where you want your server to be hosted.

![enter image description here](https://s22.postimg.cc/qf6qtdyoh/image.png)

Now give your server a name and click create. I'm calling mine "zane-test_server".

![enter image description here](https://s22.postimg.cc/m620r9dfl/image.png)

It will take a minute for your server to be created. Eventually you will see something like this:

![enter image description here](https://s22.postimg.cc/ppnyh7t1t/image.png)

Woohoo, our server is online!

## 2.) Updating and Configuring Your Server
We now need to login to our server. To do this, you need an SSH client. A popular one that I'm going to use is called PuTTY. https://www.putty.org/

Open PuTTY and enter the IP address of your newly created server into the **Host Name** box at the top of the program. You can see the IP address of my own server in the image above (138.197.196.133). The click the **Open** button.

![enter image description here](https://s22.postimg.cc/qt82tagap/image.png)

A box will popup asking you if you trust this host, click **Yes**. Now you need to enter the credentials of your new server. You can find these credentials in an email that was sent to you from DigitalOcean.

The email I received looked like this:

![enter image description here](https://s22.postimg.cc/y97cf7tr5/image.png)

You need to enter the login credentials for your server into PuTTY to connect to your server. Make sure to login as root (the administrator user in Linux systems).

![enter image description here](https://s22.postimg.cc/tbtrtlzch/image.png)

Once you login to your server, it will ask you to change your password. If all goes well, you should see something like this. You can see I'm now logged in as root, on zane-test-server.

![enter image description here](https://s22.postimg.cc/opxnlbb9d/image.png)

It's bad practice to do anything as the root user in most cases. Let's create a new username for yourself. I'm going to create a user named zane. Enter the following commands:

       adduser zane 			# create a new user named zane
       usermod -aG sudo zane	# add the user to the sudo group (gives admin privileges)
       su - zane				# switch to the new user account

You can see that I'm now logged in as my newly created user named zane.

![enter image description here](https://s22.postimg.cc/94gc1k1xt/image.png)

Let's go ahead and update the server. Enter these three commands. Press enter or type "yes" to anything that pops up during the updates.

    sudo apt-get update        # Fetches the list of available updates
	sudo apt-get upgrade       # Upgrades current packages
	sudo apt-get dist-upgrade  # Installs updates

Now your server is secured and up to date! Next time you login to your server via SSH, always use your username rather than root!

## 3.) Installing InfluxDB

Enter these five commands:

	curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
	source /etc/lsb-release
	echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
	sudo apt-get update && sudo apt-get install influxdb
	sudo systemctl enable influxdb.service
	
Start InfluxDB:

    sudo systemctl start influxdb


We will now start the InfluxDB shell. Type:

    influx

![enter image description here](https://s22.postimg.cc/ad8t11e5t/image.png)

Before we do anything, InfluxDB requires us to create an admin user (enter your own password in the below example):
    
	CREATE USER admin WITH PASSWORD 'supersecretpassword' WITH ALL PRIVILEGES

Now type:

    exit

We need to modify the InfluxDB configuration file to require password authentication. Open up the file:

    sudo nano /etc/influxdb/influxdb.conf

Scroll down to the **http** section and change **auth-enabled** to **true**, so that it looks like this:

![enter image description here](https://s22.postimg.cc/5twkfdr3l/image.png)

Now to save your changes, press **Ctrl+x**. Then type "**y**", then press enter. Restart InfluxDB using this command:

    sudo systemctl restart influxdb
Open up InfluxDB as an admin:


	influx -username admin -password supersecretpassword

Let's create a database to store our sensor data in. I'm calling mine "zane_test_db".

    
	CREATE DATABASE zane_test_db
We're all done with InfluxDB, so type exit to leave the InfluxDB shell:
			

    exit

## 4.) Installing Grafana
Enter these five commands:

	wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_5.1.4_amd64.deb
	sudo apt-get install -y adduser libfontconfig
	sudo dpkg -i grafana_5.1.4_amd64.deb
	sudo systemctl enable grafana-server.service
	sudo ufw allow 3000

Start Grafana by typing the following (you might have to enter your password):

	systemctl start grafana-server

Grafana should now be running on your server. To check it out, open your web browser and type **http://your_servers_ip_address:3000**

So to access Grafana on my own server, in my web browser I typed **http://138.197.196.133:3000**. The 3000 is needed at the end because Grafana runs on port 3000 by default.

You should see a webpage that looks like this:

![enter image description here](https://s22.postimg.cc/4mimn9101/image.png)

The default login credentials are:
Username: **admin**
Password: **admin**

Click the **Log In** button and you will be brought to the Grafana dashboard.

![enter image description here](https://s22.postimg.cc/dldae4q5t/image.png)

Go down to the bottom left of the page and click on your user icon, and then **preferences**.

![enter image description here](https://s22.postimg.cc/q002e9mr5/image.png)

Change the default admin password to whatever you'd like.

![enter image description here](https://s22.postimg.cc/fpxnf4cc1/image.png)

We need to link Grafana to our InfluxDB database. Open **Configuration->Data Sources**.

![enter image description here](https://s22.postimg.cc/swn3euidt/image.png)

Then click on the big green **Add data source** button.

![enter image description here](https://s22.postimg.cc/ucylwh5pt/image.png)

Copy these settings (use your own database name and password, of course).

![enter image description here](https://s22.postimg.cc/3rw30z8i9/image.png)

Scroll down and click on the **Save & Test** button. A green box should pop up, stating that the data source is working.

![enter image description here](https://s22.postimg.cc/rivgj565d/image.png)

## 5.) Installing Node-RED

Run these commands. They install Node-RED, InfluxDB nodes, The Things Network Nodes, and open the firewall for Node-RED.

	curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
	sudo apt-get install -y nodejs
	sudo npm install -g --unsafe-perm node-red
	sudo mkdir $HOME/.node-red
	cd $HOME/.node-red
	npm install node-red-contrib-influxdb
	npm install node-red-contrib-ttn
	sudo ufw allow 1880

Run these commands. They tell Node-Red to start when the server boots up:

	sudo npm install -g pm2
	pm2 start /usr/bin/node-red
	pm2 save
	pm2 startup systemd

Follow the instructions given from that last command. Then reboot the server to make sure everything starts up as expected. 

    sudo reboot

Your SSH session will close because the server is rebooting. Reconnect to the server using SSH again. We need to add password based authentication to Node-RED. Once you log back into the server, type:

    sudo npm install -g --unsafe-perm node-red-admin
	node-red-admin hash-pw

Enter a password to generate a password hash. Copy this password hash, we will need to use it in a second. Now you need to enable authentication by editing the Node-RED settings.js file.

    sudo nano $HOME/.node-red/settings.js

Uncomment this section of the file, and paste in your own password hash. Press **Ctrl+x**, then "y", then enter to save the changes.

![enter image description here](https://s22.postimg.cc/c5aymzp0h/image.png)

Reboot the server once again.

    sudo reboot

In your web browser, go to **http://your_servers_ip_address:1880**.

So to access Node-RED on my own server, in my web browser I typed **http://138.197.196.133:1880**. Node-RED runs on port 1880 by default. You should now see a login page that looks like this. Login to Node-RED using your password.

![enter image description here](https://s22.postimg.cc/9nz7fwsb5/image.png)

You should see the Node-RED workspace, which looks like this:

![enter image description here](https://s22.postimg.cc/aq9dynlgx/image.png)

Copy this text:

    [{"id":"dc5515ad.7db5f8","type":"ttn uplink","z":"10538cae.dc0823","name":"flbs_do_sensor","app":"8b4d3dbf.8586c","dev_id":"flbs_dissolved_oxygen_sensor","field":"","x":300,"y":320,"wires":[["b51c9fd7.46dfc","36d8720f.fb4d3e"]]},{"id":"b51c9fd7.46dfc","type":"debug","z":"10538cae.dc0823","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"true","x":550,"y":320,"wires":[]},{"id":"e303662a.f2bbf8","type":"influxdb out","z":"10538cae.dc0823","influxdb":"93fa0868.249ab8","name":"zane_test_db","measurement":"flbs_do_sensor","precision":"","retentionPolicy":"","x":760,"y":400,"wires":[]},{"id":"36d8720f.fb4d3e","type":"function","z":"10538cae.dc0823","name":"Fields and Tags","func":"msg.payload = [{\n    dissolved_oxygen: msg.payload_fields.dissolved_oxygen\n},\n{\n    sensor:\"flbs_do_sensor\",\n}];\nreturn msg;","outputs":1,"noerr":0,"x":560,"y":400,"wires":[["e303662a.f2bbf8"]]},{"id":"8b4d3dbf.8586c","type":"ttn app","z":"","appId":"flbs_test_application","accessKey":"ttn-account-v2.aT8wm22iPzA-b6SgjZ2_i3w3fIJ2FSFucJJstbmnj-o","discovery":"discovery.thethingsnetwork.org:1900"},{"id":"93fa0868.249ab8","type":"influxdb","z":"","hostname":"127.0.0.1","port":"8086","protocol":"http","database":"zane_test_db","name":"","usetls":false,"tls":""}]

Then go to the top right and click on **Import -> Clipboard**:

![enter image description here](https://s22.postimg.cc/uop90oiwx/image.png)

Now paste in that text and click the **Import** button. Four nodes will appear.

![enter image description here](https://s22.postimg.cc/n8pzf0kyp/image.png)

The two that you need to modify are the **flbs_do_sensor** node, and the **zane_test_db** node. The flbs_do_sensor node is a Things Network node, for receiving data from a sensor on The Things Network. You need to double click on it and add your own App Key, Device ID, App name, etc. You can find that out by going on The Things Network website and clicking on your application. Now click on the zane_test_db and rename everything for your own database. Also add the password needed to connect to your database. This node is for connecting to an InfluxDB database.

After you are done, click the **Deploy** button in the top right.

![enter image description here](https://s22.postimg.cc/7ah9p7j4h/image.png)

The Things Network node will show that it's connected if you entered in your Things Network information correctly.

![enter image description here](https://s22.postimg.cc/f37xh8741/image.png)

Every time a value from the dissolved oxygen sensor gets sent to The Things Network, you will see the data packet in the Node-RED debug panel. Click on the drop-down arrow by the **payload** to see the dissolved oxygen reading.

![enter image description here](https://s22.postimg.cc/x7ay1iwz5/image.png)

## 6.) Let's Make Some Graphs!

Now our server is more or less complete. We can finally use Grafana to generate some cool graphs that plot data live, as it comes in.

In a web browser, connect to your server at port 3000, once again. So for me, I'm going to http://138.197.196.133:3000. This will bring up the Grafana dashboard on your server. **Click Create->Dashboard**:

![enter image description here](https://s22.postimg.cc/5l86h2qjl/image.png)

Then select **Graph**. This will bring up an example graph with some arbitrary data, click on the top of the graph and select **Edit**.

![enter image description here](https://s22.postimg.cc/v5agn5gyp/image.png)

Copy my settings where applicable. I used the same value repeatedly for my sample data, which is why my graph is flat. In the top right, you can see that I selected a time scale of 5 minutes, and I have it refreshing the graph automatically every 5 seconds. Change those two settings to whatever you prefer. If your interval is too short (e.g. 5 minutes like in my example) then you won't see any data on the plot except for a brief period, whenever your sensor sends data.

On the bottom left is where you select your data source, as well as your device and measurement. You can see that I selected "zane_test_db" for my data source, "flbs_do_sensor" as my device, and "dissolved_oxygen" as the measurement I wanted to graph. There are many more settings you can mess with, including averaging, how to fill the sections with missing data, naming axes, etc.

![enter image description here](https://s22.postimg.cc/p718r5v8x/image.png)

After you are done, click on **Save dashboard** at the top, and give it a name.

![enter image description here](https://s22.postimg.cc/t4oigd7pt/image.png)

Your graph will now continue to update as data comes in. 

## 7.) Displaying the Graph on Your Website

The issue right now is that the graphs can only be viewed by people who have an account on your Grafana server. We want to enable anonymous viewing so that anyone on the internet can see graphs that you choose to share on your web page.

Open up the Grafana settings file:

    sudo nano /etc/grafana/grafana.ini

Scroll down to the **auth.anonymous** section, and uncomment the three related settings. Make sure to set **enabled = true**. Press Ctrl+x, then "y", then enter to save the changes.

![enter image description here](https://s22.postimg.cc/vtnwk2xf5/image.png)

Restart Grafana to see these changes:

    systemctl restart grafana-server

Open up Grafana again and go to the first graph you made. Click on **Share**.

![enter image description here](https://s22.postimg.cc/i19ho8wb5/image.png)

Now click on the **Embed**  tab and copy the text in the text box. You can see that I also changed the **Theme** to **light**, which will look better on our demo web page. Make sure to uncheck **Current time range**, so that the graph continues to update in real time. Save this text in a file for now, we will need it in the next step.

![enter image description here](https://s22.postimg.cc/o8ph2ammp/image.png)

Now we need a web page to display the graph on. Here is a Bootstrap template (Bootstrap is a web framework) we can use as our web page. Enter these commands to download and open it. 

    cd ~/
    wget https://gitlab.com/flbs_sensor_class/real_time_data_plotting_server/raw/master/index.html
    nano index.html

Now scroll down and find this section. Replace that entire iframe line with the one you copied earlier from your own graph. I would set the width around 1000, and the height around 300 to start with (like in mine below). The default size is a little too small. Once you've made the changes, save the file.

![enter image description here](https://s22.postimg.cc/c5k5evogx/image.png)

Now you want to copy this web page over to the folder that Apache (the web server we are using) serves.

    sudo cp index.html /var/www/html/

In your web browser, enter your server's IP address (for me, it is http://138.197.196.133/) and you will see your data on your web page, being hosted by your server!

If you want to modify your web page, simple go to /var/www/html/, and edit **index.html**.

![enter image description here](https://s22.postimg.cc/wadpk2ncx/image.png)

## 8.) We're done!!!

Here are some graphs created using Grafana -- for inspiration!

![enter image description here](https://s22.postimg.cc/6l7o3zx8x/image.png)